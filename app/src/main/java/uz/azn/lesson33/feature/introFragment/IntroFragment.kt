package uz.azn.lesson33.feature.introFragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.graphics.drawable.toDrawable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import org.json.JSONObject
import uz.azn.lesson32.user.User
import uz.azn.lesson33.ModelImage.ModelImage
import uz.azn.lesson33.OnItemClickListener
import uz.azn.lesson33.R
import uz.azn.lesson33.adater.HorizontalRecycleAdapter
import uz.azn.lesson33.adater.VerticalRecycleAdapter
import uz.azn.lesson33.databinding.FragmentIntroBinding
import java.io.IOException
import java.nio.charset.Charset


class IntroFragment(var mContext: Context?) : Fragment(R.layout.fragment_intro) {
    private lateinit var userList: MutableList<User>
    private lateinit var recycleAdapter: VerticalRecycleAdapter
    private lateinit var horizontalRecycleAdapter: HorizontalRecycleAdapter
    private  lateinit var modelImageList:MutableList<ModelImage>
    val REQUEST_IMAGE = 123



    init {
        mContext = context
    }

    lateinit var binding: FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        userList = arrayListOf()
        try {
            val obj = JSONObject(loadJSONFromAsset())
            val userArray = obj.getJSONArray("user")
            for (i in 0 until userArray.length()) {
                val userDetail = userArray.getJSONObject(i)
                userList.add(
                    User(
                        userDetail.getString("first_name"),
                        userDetail.getString("last_name"),
                        userDetail.getString("car"),
                        userDetail.getString("email"),
                        userDetail.getString("gender")
                    )
                )
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        recycleAdapter = VerticalRecycleAdapter(userList)
        horizontalRecycleAdapter = HorizontalRecycleAdapter(imageList())
        binding.recycleViewVertical.scrollToPosition(0)
        binding.recycleViewVertical.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, true)
        binding.recycleViewHorizantal.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,false)
        binding.recycleViewHorizantal.adapter = horizontalRecycleAdapter
        binding.recycleViewVertical.adapter = recycleAdapter
        horizontalRecycleAdapter.senOnclickItemListener(object :OnItemClickListener{
            override fun onItemClicked(position: Int) {
                if (position == 0){
                    val intent = Intent()
                    intent.type = "image/*"
                    intent.action = Intent.ACTION_GET_CONTENT
                    startActivityForResult(Intent.createChooser(intent,"Select Picture"),REQUEST_IMAGE)
                }
            }

        })

    }


    private fun loadJSONFromAsset(): String {
        val json: String?
        try {
            val inputStream = activity!!.assets.open("main_data.json")
            Log.d("TAG", "loadJSONFromAsset: $inputStream")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            val charset: Charset = Charsets.UTF_8
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, charset)

        } catch (ex: IOException) {
            ex.printStackTrace()
            return ""
        }
        return json
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    fun imageList():MutableList<ModelImage>{
        modelImageList  = arrayListOf()
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.gallery)!!,"Gallery"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.austria)!!,"Australia"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.california)!!,"California"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.indonesia)!!,"Indonesia"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.mexico)!!,"Mexico"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.thailand)!!,"Thailand"))
        modelImageList.add(ModelImage(requireContext()!!.getDrawable(R.drawable.usa)!!,"Use"))

return modelImageList
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode ==REQUEST_IMAGE && resultCode== Activity.RESULT_OK){
            if (data!=null){
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(requireContext().contentResolver, data.data)
                    val drawable = BitmapDrawable(requireContext().resources,bitmap)
                    modelImageList.add(ModelImage(drawable,"item"))
                    horizontalRecycleAdapter.notifyDataSetChanged()

                }catch (ex:IOException){
                    ex.printStackTrace()
                }
            }
        }
    }

}
