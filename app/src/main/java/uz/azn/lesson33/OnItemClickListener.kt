package uz.azn.lesson33

interface OnItemClickListener {
    fun onItemClicked(position:Int)
}