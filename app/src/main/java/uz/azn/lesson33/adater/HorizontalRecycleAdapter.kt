package uz.azn.lesson33.adater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uz.azn.lesson33.ModelImage.ModelImage
import uz.azn.lesson33.OnItemClickListener
import uz.azn.lesson33.R

class HorizontalRecycleAdapter(var imageList:MutableList<ModelImage>):RecyclerView.Adapter<HorizontalRecycleAdapter.ViewHolderHorizontal>(){
    var listImage:MutableList<ModelImage> = arrayListOf()
    private lateinit var listener:OnItemClickListener

    init {
        this.listImage = imageList

    }
    inner class ViewHolderHorizontal (itemView:View):RecyclerView.ViewHolder(itemView){
        val imageView  = itemView.findViewById<ImageView>(R.id.image_circle)
        val title_view = itemView.findViewById<TextView>(R.id.title_tv)
        fun bind(modelImage: ModelImage){
           imageView.setImageDrawable(modelImage.drawable)
            title_view.text = modelImage.title
            itemView.setOnClickListener {
                if (adapterPosition!=RecyclerView.NO_POSITION){
                listener.onItemClicked(adapterPosition)
                }
            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderHorizontal {
        val view  = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_items, parent,false)
        return ViewHolderHorizontal(view)
    }

    override fun getItemCount(): Int = listImage.size

    override fun onBindViewHolder(holder: ViewHolderHorizontal, position: Int) {
    holder.bind(listImage[position])
    }
    fun senOnclickItemListener(itemListener: OnItemClickListener){
        this.listener = itemListener
    }

}