package uz.azn.lesson33.adater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uz.azn.lesson32.user.User
import uz.azn.lesson33.R

class VerticalRecycleAdapter(list: MutableList<User>) :
    RecyclerView.Adapter<VerticalRecycleAdapter.MyViewHolder>() {

    var userList: MutableList<User> = arrayListOf()
    init {
        this.userList = list

    }

    inner class MyViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val carName = itemview.findViewById<TextView>(R.id.car_name)
        val firstName = itemview.findViewById<TextView>(R.id.firstname_tv)
        val lastName = itemview.findViewById<TextView>(R.id.lastname_tv)
        val email = itemview.findViewById<TextView>(R.id.email_tv)
        val gender = itemview.findViewById<TextView>(R.id.gender)

        fun bind(user: User) {
            carName.text = user.carname
            firstName.text = user.firstname
            lastName.text = user.lastname
            email.text = user.email
            gender.text = user.gender

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vertical_user_items, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(userList[position])
    }


}