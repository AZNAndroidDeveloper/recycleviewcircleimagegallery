package uz.azn.lesson33.ModelImage

import android.graphics.drawable.Drawable

data class ModelImage(var drawable: Drawable,var title:String) {
}